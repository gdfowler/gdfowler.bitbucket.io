MyGame.myCharacter = ((() => {
  let that = {}

  let tiles = []
  let level = null

  let cellSize = 0

  that.init = (iLevel) => {
    level = iLevel
    tiles = level === CONST_LEVELS.EASY ? MyGame.spriteService.easyTiles() : MyGame.spriteService.hardTiles()
    cellSize = CANVAS_HEIGHT / level
  }

  function checkAdjacency(cell) {
    if (cell.x - 1 > 0 && !tiles[cell.x - 1][cell.y]) {
      return true
    }
    if (cell.x + 1 < level && !tiles[cell.x + 1][cell.y]) {
      return true
    }
    if (cell.y - 1 > 0 && !tiles[cell.x][cell.y - 1]) {
      return true
    }
    if (cell.y + 1 < level && !tiles[cell.x][cell.y + 1]) {
      return true
    }
    return false
  }

  that.clicked = (e) => {
    let truePoint = { x: e.clientX - canvas.offsetLeft, y: e.clientY - canvas.offsetTop }
    let cell = { x: Math.floor(truePoint.x / cellSize), y: Math.floor(truePoint.y / cellSize) }
    console.log(`checkCellAdjacency: ${checkAdjacency(cell)}`)
  }

  that.checkTileOrder = () => {
    let inOrder = true
    let flatTiles = _.flatten(tiles)
    if (flatTiles[tiles.size - 1] !== null) {
      return false
    }
    for (let i = 1; i < flatTiles.size - 2; i++) {
      if (flatTiles[i] && flatTiles[j]) {
        if (tiles[i - 1].order > flatTiles[i].order) {
          inOrder = false
          break
        }
      }
    }
    return inOrder
  }

  that.render = () => {
    for (let i = 0; i < level; i++) {
      for (let j = 0; j < level; j++) {
        // You have to reverse these because it is built backwards
        if (tiles[j][i])
          MyGame.drawingService.drawTexture(
            tiles[j][i],
            { x: (cellSize * j) + cellSize / 2, y: (cellSize * i) + cellSize / 2 },
            0,
            { x: cellSize - 5, y: cellSize - 5 }
          )
      }
    }
  }

  return that
})())