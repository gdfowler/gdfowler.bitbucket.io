MyGame.spriteService = ((() => {
  let that = {}

  that.easyTiles = () => {
    let tiles = []

    for (let i = 0; i <= 14; i++) {
      let img = new Image()
      img.ready = false
      img.onload = () => { img.ready = true }
      img.order = i
      img.src = `images/MiniGameImages/Tile128-${i}.png`
      tiles.push(img)
    }
    tiles.push(null)
    shuffleArray(tiles)

    let tiles2d = []
    for (let i = 0; i < 4; i++) {
      let tmp = []
      for (let j = 0; j < 4; j++) {
        tmp.push(tiles[j * 4 + i])
      }
      tiles2d.push(tmp)
    }

    return tiles2d
    // return tiles
  }

  that.hardTiles = () => {
    let tiles = []

    for (let i = 0; i <= 62; i++) {
      let img = new Image()
      img.ready = false
      img.onload = () => { img.ready = true }
      img.order = i
      img.src = `images/MiniGameImages/Tile64-${i}.png`
      tiles.push(img)
    }
    tiles.push(null)
    shuffleArray(tiles)

    let tiles2d = []
    for (let i = 0; i < 8; i++) {
      let tmp = []
      for (let j = 0; j < 8; j++) {
        tmp.push(tiles[j * 8 + i])
      }
      tiles2d.push(tmp)
    }

    return tiles2d
  }

  // From https://stackoverflow.com/a/12646864/10416065
  function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    // let temp = array[array.length - 1]
    // let temp2 = array[array.length - 2]
    // array[array.length - 1] = temp2
    // array[array.length - 2] = temp
  }

  return that
})())