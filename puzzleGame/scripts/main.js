MyGame.main = (((input) => {
  let that = {}
  let lastTimeStamp = performance.now()
  let endGameLoop = false

  let myKeyboard = input.Keyboard()
  let mouseCapture = false
  let myMouse = input.Mouse()


  /**
   * Initialize the game
   */
  that.initialize = function (level) {
    endGameLoop = false
    let canvas = document.getElementById('canvas')
    console.log('game initializing...')
    lastTimeStamp = performance.now()

    // give the drawing service the canvas
    MyGame.drawingService.init(canvas)
    MyGame.fpsCounter.init()
    // MyGame.myCharacter.init(CONST_LEVELS.EASonso)
    console.log(level)
    MyGame.tileManager.init(level)

    // Create an ability to move the logo using the mouse
    myMouse.register('mousedown', function (e, elapsedTime) {
      // mouseCapture = true
      // MyGame.myCharacter.moveTo({ x: e.clientX - canvas.offsetLeft, y: e.clientY - canvas.offsetTop })
      // MyGame.myCharacter.clicked(e)
      MyGame.tileManager.clicked(e)
    })

    myMouse.register('mouseup', function (e, elapsedTime) {
      // mouseCapture = false
    })

    myMouse.register('mousemove', function (e, elapsedTime) {
      // if (mouseCapture) {
      // MyGame.myCharacter.moveTo({ x: e.clientX - canvas.offsetLeft, y: e.clientY - canvas.offsetTop })
      // }
    })

    //
    // Get the game loop started
    requestAnimationFrame(gameLoop)
  }

  /**
   * Process the registered input handlers here.
   * @param {number} elapsedTime
  */
  function processInput(elapsedTime) {
    // myKeyboard.update(elapsedTime)
    myMouse.update(elapsedTime)
    MyGame.tileManager.update(elapsedTime)
  }

  /**
   * Update the state of the "model" based upon time
   * @param {number} elapsedTime
   */
  function update(elapsedTime) {
    MyGame.fpsCounter.update(elapsedTime)
    MyGame.particleService.update(elapsedTime)
    if (MyGame.tileManager.checkWinCondition()) {
      let name = window.prompt('YOU WON!! What is your name?')
      if (name === null) name = 'Anonymous'
      let highScores = JSON.parse(localStorage.getItem('highscores'))
      highScores = highscores ? highscores : {}
      highScores[MyGame.tileManager.getLevel() + "scores"] = highscores[MyGame.tileManager.getLevel() + "scores"] ? highscores[MyGame.tileManager.getLevel() + "scores"] : []
      highScores[MyGame.tileManager.getLevel() + "times"] = highscores[MyGame.tileManager.getLevel() + "times"] ? highscores[MyGame.tileManager.getLevel() + "times"] : []
      let highscoresScore = highscores[MyGame.tileManager.getLevel() + "scores"]
      highscoresScore.push({ name: name, score: MyGame.fpsCounter.getScore(), time: MyGame.fpsCounter.getTime() })
      highscoresScore.sort((e1, e2) => { e1.score > e2.score })
      highscoresScore = highscoresScore.slice(0, 5)
      highScores[MyGame.tileManager.getLevel() + "scores"] = highscoresScore

      let highscoresTime = highscores[MyGame.tileManager.getLevel() + "times"]
      highscoresTime.push({ name: name, score: MyGame.fpsCounter.getScore(), time: MyGame.fpsCounter.getTime() })
      highscoresTime.sort((e1, e2) => { e1.time < e2.time })
      highscoresTime = highscoresTime.slice(0, 5)
      highScores[MyGame.tileManager.getLevel() + "times"] = highscoresTime

      localStorage.setItem('highscores', JSON.stringify(highscores))

      endGameLoop = true
      MyGame.menuService.showHomeScreen()
    }
    // if (MyGame.myCharacter.checkTileOrder()) {
    // console.log('tiles are in order')
    // }
    // Only we don't have anything to do here, kind of a boring game
  }

  //------------------------------------------------------------------
  //
  // Render the state of the "model", which is just our texture in this case.
  //
  //------------------------------------------------------------------
  function render() {
    MyGame.drawingService.clear()

    // MyGame.myCharacter.render()
    MyGame.tileManager.render()
    MyGame.fpsCounter.render()
    MyGame.particleService.render()
  }

  //------------------------------------------------------------------
  //
  // This is the Game Loop function!
  //
  //------------------------------------------------------------------
  function gameLoop(time) {

    let elapsedTime = time - lastTimeStamp
    lastTimeStamp = time

    processInput(elapsedTime)
    update(elapsedTime)
    render()

    if (!endGameLoop)
      requestAnimationFrame(gameLoop);
  }


  return that
})(MyGame.input))