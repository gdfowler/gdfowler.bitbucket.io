MyGame.menuService = ((() => {
  'use strict'
  let that = {}

  /* HIGH SCORES */
  let isShowingHighScores = false
  const HIGHSCORES_ID = 'highscores-parent'
  const HIGHSCORES_BUTTON = 'highscores-button'


  function updateHighScores() {
    let scores = document.getElementById('highscores-easy-moves')
    let string = ''
    let highscoresFull = JSON.parse(localStorage.getItem('highscores'))
    if (highscoresFull !== null) {
      let highscores = highscoresFull['4scores']
      highscores.forEach(e => {
        string += `<li>Name: ${e.name}, Score: ${Math.floor(e.score * 100) / 100}, Time: ${e.time}</li>`
      })
      scores.innerHTML = string

      string = ''
      scores = document.getElementById('highscores-easy-time')
      highscores = highscoresFull['4times']
      highscores.forEach(e => {
        string += `<li>Name: ${e.name}, Score: ${Math.floor(e.score * 100) / 100}, Time: ${e.time}</li>`
      })
      scores.innerHTML = string

      string = ''
      scores = document.getElementById('highscores-hard-moves')
      highscores = highscoresFull['8scores'] ? highscoresFull['8scores'] : []
      highscores.forEach(e => {
        string += `<li>Name: ${e.name}, Score: ${Math.floor(e.score * 100) / 100}, Time: ${e.time}</li>`
      })
      scores.innerHTML = string

      string = ''
      scores = document.getElementById('highscores-hard-time')
      highscores = highscoresFull['8times'] ? highscoresFull['8times'] : []
      highscores.forEach(e => {
        string += `<li>Name: ${e.name}, Score: ${Math.floor(e.score * 100) / 100}, Time: ${e.time}</li>`
      })
      scores.innerHTML = string
    }
  }

  that.showHighScores = (() => {
    updateHighScores()
    if (isShowingHighScores) {
      document.getElementById(HIGHSCORES_ID).style = 'display: none;'
      document.getElementById(HIGHSCORES_BUTTON).innerHTML = 'View High Scores'
      isShowingHighScores = false
    } else {
      document.getElementById(HIGHSCORES_ID).style = 'display: block;'
      document.getElementById(HIGHSCORES_BUTTON).innerHTML = 'Hide High Scores'
      isShowingHighScores = true
    }
  })

  /* CREDITS */
  let isShowingCredits = false
  const CREDITS_ID = 'credits'
  const CREDITS_BUTTON = 'credits-button'

  that.showCredits = (() => {
    if (isShowingCredits) {
      document.getElementById(CREDITS_ID).style = 'display: none;'
      document.getElementById(CREDITS_BUTTON).innerHTML = 'View Credits'
      isShowingCredits = false
    } else {
      document.getElementById(CREDITS_ID).style = 'display: block;'
      document.getElementById(CREDITS_BUTTON).innerHTML = 'Hide Credits'
      isShowingCredits = true
    }
  })

  /* KEYBINDINGS */
  let isShowingKeybindings = false
  const KEYBINDINGS_ID = 'keybindings'
  const KEYBINDINGS_BUTTON = 'keybindings-button'
  let currAction = undefined
  let blackListKeys = ['Escape']
  let keybindings = JSON.parse(localStorage.getItem('keybindings'))

  if (keybindings === null) {
    keybindings = {
      up: 'ArrowUp',
      down: 'ArrowDown',
      left: 'ArrowLeft',
      right: 'ArrowRight',
      rotateLeft: 'z',
      rotateRight: 'c',
    }
    localStorage.setItem('keybindings', JSON.stringify(keybindings))
  }

  that.setKeybindings = function (action) {
    document.getElementById('keybindings-hint').style = 'display: block'
    currAction = action
    window.addEventListener('keydown', keybindFunc)
  }

  function keybindFunc(event) {
    if (blackListKeys.indexOf(event.key) == -1 && currAction != undefined) {
      document.getElementById('keybindings-hint').style = 'display: none'
      keybindings[currAction] = event.key
      currAction = undefined
      updateKeybindings()
      window.removeEventListener('keydown', keybindFunc)
    }
  }

  function updateKeybindings() {
    localStorage.setItem('keybindings', JSON.stringify(keybindings))
    document.getElementById('up-key').innerHTML = keybindings['up']
    document.getElementById('down-key').innerHTML = keybindings['down']
    document.getElementById('left-key').innerHTML = keybindings['left']
    document.getElementById('right-key').innerHTML = keybindings['right']
    document.getElementById('rLeft-key').innerHTML = keybindings['rotateLeft']
    document.getElementById('rRight-key').innerHTML = keybindings['rotateRight']
  }

  that.showKeybindings = (() => {
    updateKeybindings()
    if (isShowingKeybindings) {
      document.getElementById(KEYBINDINGS_ID).style = 'display: none;'
      document.getElementById(KEYBINDINGS_BUTTON).innerHTML = 'Set Keybindings'
      isShowingKeybindings = false
    } else {
      document.getElementById(KEYBINDINGS_ID).style = 'display: block;'
      document.getElementById(KEYBINDINGS_BUTTON).innerHTML = 'Hide Keybindings'
      isShowingKeybindings = true
    }
  })

  that.showGameScreen = ((level) => {
    MyGame.main.initialize(level)
    document.getElementById('menu').style = "display: none;"
    document.getElementById('gameArea').style = "display: block;"
  })

  that.showHomeScreen = (() => {
    document.getElementById('menu').style = "display: block;"
    document.getElementById('gameArea').style = "display: none;"
  })

  return that
})())