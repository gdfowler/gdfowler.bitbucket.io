MyGame.tileManager = ((() => {
  let that = {}
  let tiles = []
  let level = null
  let cellSize = 0

  let isAnimating = false
  let nullTile = { x: null, y: null }

  that.getLevel = () => { return level }

  that.init = (iLevel) => {
    level = iLevel
    cellSize = CANVAS_HEIGHT / level
    tiles = generateTiles(level)
  }

  function generateTiles(size) {
    let imgTiles = size === CONST_LEVELS.EASY ? MyGame.spriteService.easyTiles() : MyGame.spriteService.hardTiles()
    let tiles = []

    for (let i = 0; i < size; i++) {
      let tmp = []
      for (let k = 0; k < size; k++) {
        if (imgTiles[i][k])
          tmp.push(MyGame.tile(
            {
              imgSrc: imgTiles[i][k],
              order: imgTiles[i][k].order,
              center: { x: (cellSize * i) + cellSize / 2, y: (cellSize * k) + cellSize / 2 },
              size: { x: cellSize - 5, y: cellSize - 5 }
            }
          ))
        else {
          tmp.push(null)
          nullTile = { x: i, y: k }
        }
      }
      tiles.push(tmp)
    }

    return tiles
  }

  that.checkWinCondition = () => {
    if (tiles[level - 1][level - 1]) {
      return false
    }
    let correct = true
    for (let i = 0; i < level; i++) {
      for (let k = 0; k < level; k++) {
        if (tiles[i][k]) {
          if (!that.checkCellCorrectness(tiles[i][k].getCenter(), tiles[i][k].order, true))
            correct = false
        }
      }
    }
    return correct
  }

  that.checkCellCorrectness = (center, order, noSparks) => {
    if (Math.floor(center.y / cellSize) * level + Math.floor(center.x / cellSize) === order) {
      if (noSparks === undefined)
        MyGame.particleService.goodFit(center, cellSize)
      return true
    }
    return false
  }

  that.clicked = (e) => {
    if (!isAnimating) {
      let truePoint = { x: e.clientX - canvas.offsetLeft, y: e.clientY - canvas.offsetTop }
      let cell = { x: Math.floor(truePoint.x / cellSize), y: Math.floor(truePoint.y / cellSize) }
      let adjCheck = checkAdjacency(cell)
      if (adjCheck) {
        tiles[cell.x][cell.y].moveTo(adjCheck, cellSize)
        isAnimating = true
        tiles[nullTile.x][nullTile.y] = tiles[cell.x][cell.y]
        tiles[cell.x][cell.y] = null
        nullTile = { x: cell.x, y: cell.y }
      }
    }
  }

  that.doneAnimating = () => {
    isAnimating = false
  }

  function checkAdjacency(cell) {
    if (cell.x - 1 === nullTile.x && cell.y === nullTile.y) {
      return DIRECTIONS.LEFT
    }
    if (cell.x + 1 === nullTile.x && cell.y === nullTile.y) {
      return DIRECTIONS.RIGHT
    }
    if (cell.x === nullTile.x && cell.y - 1 === nullTile.y) {
      return DIRECTIONS.UP
    }
    if (cell.x === nullTile.x && cell.y + 1 === nullTile.y) {
      return DIRECTIONS.DOWN
    }
    return false
  }

  that.update = (elapsedTime) => {
    for (let i = 0; i < level; i++) {
      for (let k = 0; k < level; k++) {
        if (tiles[i][k])
          tiles[i][k].update(elapsedTime)
      }
    }
  }

  that.render = () => {
    for (let i = 0; i < level; i++) {
      for (let k = 0; k < level; k++) {
        if (tiles[i][k])
          tiles[i][k].render()
      }
    }
  }

  return that
})())
