MyGame.fpsCounter = (() => {
  let that = {}
  let counter = 0
  let time_start = 0
  let time_now = 0
  let fpsHTML = null
  let timeHTML = null
  let numMovesHTML = null
  let numMoves = 0

  that.init = () => {
    fpsHTML = document.getElementById('fps')
    timeHTML = document.getElementById('timestamp')
    numMovesHTML = document.getElementById('numMoves')
    counter = 0
    time_start = performance.now()
    time_now = time_start
    numMoves = 0
  }

  that.update = (elapsedTime) => {
    time_now += elapsedTime
    counter += 1
  }

  that.incrementNumMoves = () => {
    numMoves += 1
  }

  that.getTime = () => { return Math.floor((counter / ((time_now - time_start) / 1000)) * 100) / 100 }
  that.getScore = () => { return numMoves }

  that.render = () => {
    fpsHTML.innerHTML = Math.floor((counter / ((time_now - time_start) / 1000)) * 100) / 100
    timeHTML.innerHTML = Math.round((time_now - time_start) / 1000)
    numMovesHTML.innerHTML = numMoves
  }

  return that
})()