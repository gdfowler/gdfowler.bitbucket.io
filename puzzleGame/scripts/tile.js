MyGame.tile = (spec) => {
  let that = {}

  let image = spec.imgSrc
  that.order = spec.order
  let center = spec.center
  let size = spec.size

  let targetDirection = null
  let target = null

  let moveRate = 350

  that.getCenter = () => {
    return center
  }

  that.update = (elapsedTime) => {
    if (targetDirection) {
      switch (targetDirection) {
        case DIRECTIONS.UP:
          center.y -= moveRate / 1000 * elapsedTime
          if (target.y > center.y) {
            center.y = target.y
            target = null
            targetDirection = null
            MyGame.tileManager.doneAnimating()
            MyGame.tileManager.checkCellCorrectness(center, that.order)
          }
          break;
        case DIRECTIONS.DOWN:
          center.y += moveRate / 1000 * elapsedTime
          if (target.y < center.y) {
            center.y = target.y
            target = null
            targetDirection = null
            MyGame.tileManager.doneAnimating()
            MyGame.tileManager.checkCellCorrectness(center, that.order)
          }
          break;
        case DIRECTIONS.LEFT:
          center.x -= moveRate / 1000 * elapsedTime
          if (target.x > center.x) {
            center.x = target.x
            target = null
            targetDirection = null
            MyGame.tileManager.doneAnimating()
            MyGame.tileManager.checkCellCorrectness(center, that.order)
          }
          break;
        case DIRECTIONS.RIGHT:
          center.x += moveRate / 1000 * elapsedTime
          if (target.x < center.x) {
            center.x = target.x
            target = null
            targetDirection = null
            MyGame.tileManager.doneAnimating()
            MyGame.tileManager.checkCellCorrectness(center, that.order)
          }
          break;
      }
    }
    // if (target) {
    //   if (target.x < center.x) {
    //     center.x -= moveRate / 1000 * elapsedTime
    //     if (target.x > center.x) {
    //       center.x = target.x
    //       target = null
    //       MyGame.tileManager.doneAnimating()
    //     }
    //   }
    //   if (target.x > center.x) {
    //     center.x += moveRate / 1000 * elapsedTime
    //     if (target.x < center.x) {
    //       center.x = target.x
    //       target = null
    //       MyGame.tileManager.doneAnimating()
    //     }
    //   }
    //   if (target.y < center.y) {
    //     center.y -= moveRate / 1000 * elapsedTime
    //     if (target.y > center.y) {
    //       center.y = target.y
    //       target = null
    //       MyGame.tileManager.doneAnimating()
    //     }
    //   }
    //   if (target.y > center.y) {
    //     center.y += moveRate / 1000 * elapsedTime
    //     if (target.y < center.y) {
    //       center.y = target.y
    //       target = null
    //       MyGame.tileManager.doneAnimating()
    //     }
    //   }
    // }
  }

  that.moveTo = (direction, cellSize) => {
    MyGame.fpsCounter.incrementNumMoves()
    targetDirection = direction
    switch (targetDirection) {
      case DIRECTIONS.UP:
        target = { x: center.x, y: center.y - cellSize }
        break;
      case DIRECTIONS.DOWN:
        target = { x: center.x, y: center.y + cellSize }
        break;
      case DIRECTIONS.LEFT:
        target = { x: center.x - cellSize, y: center.y }
        break;
      case DIRECTIONS.RIGHT:
        target = { x: center.x + cellSize, y: center.y }
        break;
    }
  }

  that.render = () => {
    MyGame.drawingService.drawTexture(
      image,
      center,
      0,
      size
    )
  }

  return that
}