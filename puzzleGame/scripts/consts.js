const CONST_LEVELS = {
  EASY: 4,
  HARD: 8
}
const CANVAS_HEIGHT = 900
const CANVAS_WIDTH = 900

const DIRECTIONS = {
  UP: 'up',
  DOWN: 'down',
  LEFT: 'left',
  RIGHT: 'right'
}