MyGame.myCharacter = (((img) => {
  let that = {}

  let image = new Image()
  image.ready = false
  image.onload = () => { image.ready = true }
  image.src = img

  // Position and size
  let center = { x: 100, y: 100 }
  let rotation = 0
  let size = { x: 100, y: 100 }

  // Rates
  let moveRate = 200
  let rotationRate = Math.PI

  that.init = () => {
    center = { x: 100, y: 100 }
    size = { x: 100, y: 100 }
    rotation = 0
    moveRate = 200
    rotationRate = Math.PI
  }

  that.moveUp = (elapsedTime) => {
    center.y -= moveRate * elapsedTime / 1000
  }
  that.moveDown = (elapsedTime) => {
    center.y += moveRate * elapsedTime / 1000
  }
  that.moveLeft = (elapsedTime) => {
    center.x -= moveRate * elapsedTime / 1000
  }
  that.moveRight = (elapsedTime) => {
    center.x += moveRate * elapsedTime / 1000
  }
  that.rotateLeft = (elapsedTime) => {
    rotation -= rotationRate * elapsedTime / 1000
  }
  that.rotateRight = (elapsedTime) => {
    rotation += rotationRate * elapsedTime / 1000
  }

  that.moveTo = (newCenter) => {
    center.x = newCenter.x
    center.y = newCenter.y
  }

  that.update = () => { }

  that.render = () => {
    MyGame.drawingService.drawTexture(image, center, rotation, size)
  }

  return that
})('images/USU-Logo.png'))