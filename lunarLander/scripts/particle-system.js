const PARTICLES_PER_THRUST = 15
const PARTICLES_PER_CRASH = 90
const THRUST_PARTICLE_ANGLE = (2 / 3) * Math.PI

/**
 * Taken from code from Dean Mathias for USU course CS5410
 */
let particleSystem = (() => {
    let that = {}
    let particles = []
    let EXPLOSION_COLORS = [
        "rgb(102, 51, 0)",  // Brown
        "rgb(255, 0, 0)",   // Red
        "rgb(255, 153, 0)", // Orange
        "rgb(255, 255, 0)"  // Yellow
    ]

    /**
     * Spec should look like:
     * {
     *    center: {x: 0, y: 0},
     *    radius: 0,
     *    fill: "green",
     *    speed: 0,
     *    lifetime: 0
     * }
     */
    function create(spec) {
        let that = {}

        spec.alive = 0

        that.update = (elapsedTime) => {
            spec.center.x += (spec.speed * spec.direction.x * elapsedTime);
            spec.center.y += (spec.speed * spec.direction.y * elapsedTime);
            spec.alive += elapsedTime;

            return spec.alive < spec.lifetime;
        }

        that.render = () => {
            drawingService.fillCircle(spec.center.x, spec.center.y, spec.radius, spec.fill)
        }

        return that
    }

    that.shipThrust = (character) => {
        for (let i = 0; i < PARTICLES_PER_THRUST; i++) {
            let spec = {
                center: {
                    x: character.center.x,
                    y: character.center.y,
                },
                radius: 1,
                fill: EXPLOSION_COLORS[Random.nextRange(1, 3)],
                speed: Math.abs(Random.nextGaussian(0, 0.001)),
                direction: Random.nextCircleVectorRange(character.rotation),
                lifetime: Random.nextGaussian(100000, 250)
            }
            let particle = create(spec)
            particles.push(particle)
        }
    }

    that.shipCrash = (character) => {
        for (let i = 0; i < PARTICLES_PER_CRASH; i++) {
            let spec = {
                center: {
                    x: character.center.x,
                    y: character.center.y,
                },
                radius: 2,
                fill: EXPLOSION_COLORS[Random.nextRange(0, 3)],
                speed: Random.nextGaussian(0, 0.0003),
                direction: Random.nextCircleVector(),
                lifetime: Random.nextGaussian(500000, 250)
            }
            let particle = create(spec)
            particles.push(particle)
        }
    }

    that.update = (elapsedTime) => {
        let newParticles = []
        for (let i = 0; i < particles.length; i++) {
            if (particles[i].update(elapsedTime)) {
                newParticles.push(particles[i])
            }
        }
        particles = newParticles
    }

    that.render = () => {
        for (let i = 0; i < particles.length; i++) {
            particles[i].render()
        }
    }

    return that
})()