let drawingService = (() => {
  let that = {}
  let canvas = null
  let ctx = null

  that.init = (canvasElement) => {
    canvas = canvasElement
    ctx = canvas.getContext('2d')
  }

  that.clear = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }

  that.clearBlack = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.fillStyle = "#000"
    ctx.fillRect(0, 0, canvas.width, canvas.height)
  }

  that.setStrokeStyle = (strokeStyle) => {
    ctx.setStrokeStyle = strokeStyle
  }

  that.drawFillSquare = (x, y, width, height) => {
    ctx.fillRect(x, y, width, height)
  }

  that.drawOutlineSquare = (x, y, width, height, lineWidth) => {
    if (lineWidth) ctx.lineWidth = lineWidth
    ctx.strokeRect(x, y, width, height)
  }

  /**
   * Expecting an array of objects
   * EX: [{x:1, y:1}, {x:2, y:5}, {x:9, y:33}]
   */
  that.drawPath = (path, useClosePath) => {
    ctx.save()
    ctx.lineWidth = 5
    ctx.strokeStyle = "#fff"
    ctx.fillStyle = "#000"
    ctx.beginPath()
    ctx.moveTo(path[0].x, path[0].y)
    for (let i = 1; i < path.length; i++) {
      ctx.lineTo(path[i].x, path[i].y)
    }

    ctx.lineTo(CANVAS_WIDTH, CANVAS_HEIGHT)
    ctx.lineTo(0, CANVAS_HEIGHT)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
  }

  that.drawPathSafe = (path) => {
    let safe = []
    for (let i = 0; i < path.length; i++) {
      if (path[i].safe) {
        safe.push([path[i], path[i + 1]])
        i++
      }
    }
    for (let j = 0; j < safe.length; j++) {
      ctx.lineWidth = 7
      ctx.strokeStyle = "#0fb300"
      ctx.beginPath()
      ctx.moveTo(safe[j][0].x, safe[j][0].y)
      ctx.lineTo(safe[j][1].x, safe[j][1].y)
      ctx.stroke()
    }
    ctx.restore()
  }

  that.strokeText = (text, x, y, maxWidth, font) => {
    ctx.font = font
    if (maxWidth) ctx.strokeText(text, x, y, maxWidth)
    else ctx.strokeText(text, x, y)
  }

  that.fillText = (text, x, y, maxWidth, font, style) => {
    ctx.fillStyle = style
    ctx.font = font
    if (maxWidth) ctx.fillText(text, x, y, maxWidth)
    else ctx.fillText(text, x, y)
  }

  that.drawCircle = (x, y, radius) => {
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI)
    ctx.stroke()
  }

  that.fillCircle = (x, y, radius, fillStyle) => {
    ctx.save()
    ctx.beginPath()
    ctx.arc(x, y, radius, 0, 2 * Math.PI)
    ctx.fillStyle = fillStyle
    ctx.fill()
    ctx.restore()
  }

  that.drawArc = (x, y, radius, startAngle, endAngle, counterClockwise) => {
    if (!counterClockwise) counterClockwise = false
    if (!startAngle) startAngle = 0
    if (!endAngle) endAngle = 2 * Math.PI
    ctx.beginPath()
    ctx.arc(x, y, radius, startAngle, endAngle, counterClockwise)
    ctx.stroke()
  }

  /** Draws a texture to the canvas with the following specification:
      image: Image
  */
  that.drawBackground = (image) => {
    if (image.ready)
      ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
  }

  /** Draws a texture to the canvas with the following specification:
  *
  *    image: Image
  *    center: {x: , y: }
  *    rotation: radians
  *    size: { width: , height: }
  */
  that.drawTexture = (image, center, rotation, size) => {
    if (image.ready) {
      ctx.save();

      ctx.translate(center.x, center.y);
      ctx.rotate(rotation);
      ctx.translate(-center.x, -center.y);

      ctx.drawImage(
        image,
        center.x - size.x / 2,
        center.y - size.y / 2,
        size.x, size.y);

      ctx.restore();
    }
  }

  return that
})()