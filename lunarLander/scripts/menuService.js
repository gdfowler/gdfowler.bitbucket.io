let isShowingHighScores = false
let isShowingCredits = false
let isShowingKeybindings = false

let blackListKeys = ['Escape']

let keybindings = JSON.parse(localStorage.getItem('keybindings'))

if (keybindings === null) {
  keybindings = {
    down: 'ArrowDown',
    left: 'ArrowLeft',
    right: 'ArrowRight'
  }
  localStorage.setItem('keybindings', JSON.stringify(keybindings))
}

function exitOnEsc(event) {
  if (event.key === 'Escape') {
    exitToMenu()
  }
}

function updateHighScores() {
  let scores = document.getElementById('highscores')
  let string = ''
  let highscores = JSON.parse(localStorage.getItem('highscores'))
  if (highscores !== null) {
    highscores.sort((e1, e2) => e1.score < e2.score)
    highscores.forEach(e => {
      let newDate = new Date(e.date)
      let dateString = newDate.getFullYear() + "-" + newDate.getMonth() + "-" + newDate.getDay()
      string += `<li>Name: ${e.name}, Score: ${Math.floor(e.score * 100) / 100}, Date: ${dateString}</li>`
    })
    scores.innerHTML = string
  }
}

function showHighScores() {
  updateHighScores()
  if (isShowingHighScores) {
    document.getElementById('highscores-parent').style = 'display: none;'
    document.getElementById('highscores-button').innerHTML = 'View High Scores'
    isShowingHighScores = false
  } else {
    document.getElementById('highscores-parent').style = 'display: block;'
    document.getElementById('highscores-button').innerHTML = 'Hide High Scores'
    isShowingHighScores = true
  }
}

function showCredits() {
  if (isShowingCredits) {
    document.getElementById('credits').style = 'display: none;'
    document.getElementById('credits-button').innerHTML = 'View Credits'
    isShowingCredits = false
  } else {
    document.getElementById('credits').style = 'display: block;'
    document.getElementById('credits-button').innerHTML = 'Hide Credits'
    isShowingCredits = true
  }
}

function showKeybindings() {
  updateKeybindings()
  if (isShowingKeybindings) {
    document.getElementById('keybindings').style = 'display: none;'
    document.getElementById('keybindings-button').innerHTML = 'Set Keybindings'
    isShowingKeybindings = false
  } else {
    document.getElementById('keybindings').style = 'display: block;'
    document.getElementById('keybindings-button').innerHTML = 'Hide Keybindings'
    isShowingKeybindings = true
  }
}

function menuStartGame() {
  document.getElementById('menu').style = "display: none;"
  document.getElementById('gameArea').style = "display: block;"
  // TODO: Add if you can get it to work
  // window.addEventListener('keydown', exitOnEsc)
  initializeFullGame()
  initialize()
  requestAnimationFrame(gameLoop)
  // Start the game here
}

function exitToMenu() {
  endGameLoop()
  // let answer = window.confirm("Do you really want to leave?")
  // if (answer) {
  document.getElementById('menu').style = "display: block;"
  document.getElementById('gameArea').style = "display: none;"
  // TODO: Add if you can get it to work
  // window.removeEventListener('keydown', exitOnEsc)
  // } else {
  //   resumeGameLoop()
  // }
}

let currAction = undefined
function setKeybindings(action) {
  document.getElementById('keybindings-hint').style = 'display: block'
  currAction = action
  window.addEventListener('keydown', keybindFunc)
}

function keybindFunc(event) {
  if (blackListKeys.indexOf(event.key) == -1 && currAction != undefined) {
    document.getElementById('keybindings-hint').style = 'display: none'
    keybindings[currAction] = event.key
    currAction = undefined
    updateKeybindings()
    window.removeEventListener('keydown', keybindFunc)
  }
}


function updateKeybindings() {
  localStorage.setItem('keybindings', JSON.stringify(keybindings))
  document.getElementById('down-key').innerHTML = keybindings['down']
  document.getElementById('left-key').innerHTML = keybindings['left']
  document.getElementById('right-key').innerHTML = keybindings['right']
}