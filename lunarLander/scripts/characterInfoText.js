let characterText = (() => {
  let that = {}
  that.fuel = 0
  that.speed = 0
  that.angle = 0

  that.reset = () => {
    that.fuel = 0
    that.speed = 0
    that.angle = 0
  }

  that.update = (character) => {
    if (!character.dead) {
      that.fuel = Math.floor(character.fuel)
      that.speed = Math.floor(character.speed * 10) / 10
      that.angle = Math.floor(character.rotation * (180 / Math.PI)) % 360
      if (that.angle < 0) that.angle = 360 + that.angle
    } else {
      that.fuel = 0
      that.speed = 0
      that.angle = 0
    }
  }

  that.render = () => {
    drawingService.fillText(`FUEL: ${that.fuel}`, 900, 100, 100, '28px consolas', (that.fuel > 0 ? 'lightgreen' : 'white'))
    drawingService.fillText(`SPEED: ${that.speed}`, 900, 150, 100, '28px consolas', (that.speed <= 2 ? 'lightgreen' : 'white'))
    drawingService.fillText(`ANGLE: ${that.angle}`, 900, 200, 100, '28px consolas', (that.angle < 5 || that.angle > 355 ? 'lightgreen' : 'white'))
  }

  return that
})()