function generateTerrain(numPlatforms) {
  const MAX_ITERATIONS = 7
  const PLATFORM_WIDTH = Math.floor(CANVAS_WIDTH / 5)
  const MAX_TERRAIN_HEIGHT = Math.floor(CANVAS_HEIGHT / 1.7)
  let terrain = [];
  let farRight = Random.nextRange(MAX_TERRAIN_HEIGHT, CANVAS_HEIGHT)
  let farLeft = Random.nextRange(MAX_TERRAIN_HEIGHT, CANVAS_HEIGHT)
  terrain.push({ x: 0, y: farLeft, safe: false })
  let platformLeftBound = 100
  let platformRightBound = 0
  if (numPlatforms > 1)
    platformRightBound = (CANVAS_WIDTH / 2) - PLATFORM_WIDTH
  else platformRightBound = CANVAS_WIDTH - PLATFORM_WIDTH
  for (let i = 0; i < numPlatforms; i++) {
    let x = Random.nextRange(platformLeftBound, platformRightBound)
    let y = Random.nextRange(MAX_TERRAIN_HEIGHT, CANVAS_HEIGHT)
    platformLeftBound = x + PLATFORM_WIDTH
    platformRightBound = CANVAS_WIDTH - PLATFORM_WIDTH
    terrain.push({ x: x, y: y, safe: true })
    terrain.push({ x: x + PLATFORM_WIDTH, y: y, safe: false })
  }
  terrain.push({ x: CANVAS_WIDTH, y: farRight, safe: false })

  for (let a = 0; a < MAX_ITERATIONS; a++) {
    let roughness = (numPlatforms === 1 ? a / 2 : a)
    for (let i = 0; i < terrain.length - 1; i++) {
      if (!terrain[i].safe) {
        let x1 = terrain[i].x
        let y1 = terrain[i].y
        let x2 = terrain[i + 1].x
        let y2 = terrain[i + 1].y
        let y = terrainElevation(x1, y1, x2, y2, roughness)
        while (y < MAX_TERRAIN_HEIGHT)
          y = terrainElevation(x1, y1, x2, y2, roughness)
        let newPoint = { x: x1 + ((x2 - x1) / 2), y: y, safe: false }
        terrain.splice(i + 1, 0, newPoint)
        i++
      }
    }
  }
  return terrain
}

function terrainElevation(x1, y1, x2, y2, roughness) {
  r = roughness * Random.nextGaussian(0, 1) * Math.abs(x2 - x1)
  y = (1 / 2) * (y1 + y2) + r
  return y
}