const CANVAS_WIDTH = 1024
const CANVAS_HEIGHT = 1024
const STARTER_FUEL = 100
const GRAVITY = -9.8 / 300000
const THRUST_PER_MS = -10 / 40000
const COUNT_DOWN_START = 4 * 1000
const COLLISION_TYPE = {
  NONE: 'NONE',
  GOOD: 'GOOD',
  BAD: 'BAD'
}
const DEATH_COUNTER_LENGTH = 5000

let deathCounter = DEATH_COUNTER_LENGTH
let countDownNumber = COUNT_DOWN_START
let score = 0

let canvas = null
let ctx = null

let continueGameLoop = true

let startTime = 0
let time = 0
let prevTime = 0
let numPlatforms = 2

let x = 200
let y = 10
let terrain = []

let processInput = processGameInput
let update = gameUpdate
let render = gameRender

let wonAudio = new Audio('audio/VictorySmall.ogg')
wonAudio.ready = false
wonAudio.addEventListener('canplay', () => { wonAudio.ready = true })
wonAudio.volume = 0.5

let thrustAudio = new Audio('audio/Driving.ogg')
thrustAudio.ready = false
thrustAudio.addEventListener('canplay', () => { thrustAudio.ready = true })
thrustAudio.volume = 0.7

let explosionAudio = new Audio('audio/Explosion1.wav')
explosionAudio.ready = false
explosionAudio.addEventListener('canplay', () => { explosionAudio.ready = true })
explosionAudio.volume = 0.5

let backgroundImg = new Image()
backgroundImg.src = 'img/background.png'
backgroundImg.ready = false
backgroundImg.onload = () => backgroundImg.ready = true

let fireBallImg = new Image()
fireBallImg.src = 'img/fireball.png'
fireBallImg.ready = false
fireBallImg.onload = () => fireBallImg.ready = true

// let particleSystem = ParticleSystem(drawingService, {
//   image: fireBallImg,
//   center: { x: 300, y: 300 },
//   size: { mean: 10, stdev: 3 },
//   speed: { mean: 0, stdev: 0.01 },
//   lifetime: { mean: 10000, stdev: 250 }
// })

let myCharacter = (imgPath => {
  let that = {}
  that.center = { x: CANVAS_WIDTH / 2, y: 200 }
  that.radius = 30
  that.momentum = { x: 0, y: 0 }
  that.rotation = Math.PI
  that.speed = 0
  that.fuel = STARTER_FUEL
  that.dead = false
  that.characterImg = new Image()
  that.characterImg.src = imgPath
  that.characterImg.ready = false
  that.characterImg.onload = () => that.characterImg.ready = true

  that.characterCircle = () => {
    return { center: { x: that.center.x, y: that.center.y }, radius: that.radius }
  }

  that.stable = () => {
    let newAngle = Math.floor(that.rotation * (180 / Math.PI)) % 360
    if (newAngle < 0) newAngle = 360 + newAngle
    return that.fuel > 0 && that.speed <= 2 && (newAngle < 5 || newAngle > 355)
  }

  that.die = () => {
    that.dead = true
  }

  that.reset = () => {
    that.center = { x: CANVAS_WIDTH / 2, y: 200 }
    that.momentum = { x: 0, y: 0 }
    that.rotation = Math.PI / 2
    that.speed = 0
    that.fuel = STARTER_FUEL
    that.dead = false
  }

  that.update = (elapsedTime, collisionType) => {
    if (!that.dead) {
      // if (collisionType === COLLISION_TYPE.NONE) {
      that.momentum.y -= (GRAVITY * elapsedTime)
      let oldX = that.center.x
      let oldY = that.center.y
      that.center.x += that.momentum.x * elapsedTime
      that.center.y += that.momentum.y * elapsedTime
      that.speed = Math.sqrt(Math.pow((oldX - that.center.x), 2) + Math.pow((oldY - that.center.y), 2)) / elapsedTime * 20
      // } else {
      //   that.center.x = CANVAS_WIDTH * 2
      //   that.center.y = CANVAS_HEIGHT * 2
      // }
    }
  }

  that.render = () => {
    if (!that.dead) {
      // drawingService.drawCircle(that.center.x, that.center.y, that.radius)
      drawingService.drawTexture(
        that.characterImg,
        that.center,
        that.rotation,
        { x: 50, y: 50 }
      )
    }
  }

  that.fireThrust = (elapsedTime) => {
    if (thrustAudio.ready)
      thrustAudio.play()
    if (!that.dead)
      particleSystem.shipThrust(that)
    // let rads = that.rotation * Math.PI / 180
    let thrustX = Math.sin(that.rotation)
    let thrustY = Math.cos(that.rotation)

    that.momentum.x += (thrustX * -THRUST_PER_MS * elapsedTime)
    that.momentum.y += (thrustY * THRUST_PER_MS * elapsedTime)
    that.fuel -= elapsedTime * 0.015
  }
  that.rotateLeft = (elapsedTime) => {
    that.rotation = that.rotation - (elapsedTime / 500)
    that.rotation = that.rotation % (2 * Math.PI)
  }
  that.rotateRight = (elapsedTime) => {
    that.rotation = that.rotation + (elapsedTime / 500)
    that.rotation = that.rotation % (2 * Math.PI)
  }

  return that
})("img/landeranim.png")


function initializeKeybindings() {
  let currKeybindings = JSON.parse(localStorage.getItem('keybindings'))
  // currKeybindings.forEach(e => { console.log(e) })
  for (var e in currKeybindings) {
    if (e === 'down')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.fireThrust)
    if (e === 'left')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.rotateLeft)
    if (e === 'right')
      inputEvents.registerCommand(currKeybindings[e], myCharacter.rotateRight)
  }
}

function handleKeyDown(event) {
  inputEvents.addToBuffer(event)
}

function handleKeyUp(event) {
  inputEvents.removeFromBuffer(event)
}

function removeListeners() {
  window.removeEventListener('keydown', handleKeyDown)
  window.removeEventListener('keyup', handleKeyUp)
}

function initializeFullGame() {
  numPlatforms = 2
}

function initialize() {
  initializeKeybindings()
  countDownNumber = COUNT_DOWN_START
  continueGameLoop = true
  deathCounter = DEATH_COUNTER_LENGTH
  // get canvas
  canvas = document.getElementById('canvas')
  ctx = canvas.getContext('2d')
  drawingService.init(document.getElementById('canvas'))

  // set up event listeners
  window.addEventListener('keydown', handleKeyDown)
  window.addEventListener('keyup', handleKeyUp)
  terrain = generateTerrain(numPlatforms)

  // init fps counter
  fpsCounter.init()
  fpsCounter.updateTimeStart()
  startTime = performance.now()
  prevTime = performance.now()

  myCharacter.reset()
  characterText.reset()

  // start the game loop
}

function endGameLoop() {
  continueGameLoop = false
}

function collisionGood() {
  wonAudio.play()
  score += myCharacter.fuel
  numPlatforms -= 1
  if (numPlatforms === 0)
    endGameFinal()
  else {
    render = renderCountdown
    update = countdownUpdate
  }
}

function collisionBad() {
  explosionAudio.play()
  particleSystem.shipCrash(myCharacter)
  myCharacter.die()
}

function resumeGameLoop() {
  console.log('here')
  continueGameLoop = true
  // prevTime = performance.now() - 15
  requestAnimationFrame(gameLoop)
}

function processGameInput(elapsedTime) {
  inputEvents.processInput(elapsedTime)
}

function gameUpdate(timestamp, elapsedTime) {
  if (myCharacter.dead) {
    if (deathCounter < 0)
      exitToMenu()
    deathCounter -= elapsedTime
  }
  let collided = runCollisionDetection(terrain, myCharacter.characterCircle())
  fpsCounter.incrementCounter()
  fpsCounter.updateTimeNow(timestamp)
  myCharacter.update(elapsedTime, collided)
  characterText.update(myCharacter)
  particleSystem.update(timestamp)
  if (collided === COLLISION_TYPE.GOOD) {
    if (myCharacter.stable()) {
      collisionGood()
    } else {
      if (!myCharacter.dead)
        collisionBad()
    }
  }
  else if (myCharacter.fuel < 0 || collided === COLLISION_TYPE.BAD) {
    if (!myCharacter.dead)
      collisionBad()
  }
}

function countdownUpdate(timestamp, elapsedTime) {
  countDownNumber -= elapsedTime
  if (countDownNumber < 0) {
    render = gameRender
    update = gameUpdate
    initialize()
    // start new game
  }
}

function endGameFinal() {
  let name = window.prompt('What is your name?')
  if (name === null) name = 'Anonymous'
  let highscores = JSON.parse(localStorage.getItem('highscores'))
  highscores = highscores ? highscores : []
  highscores.push({ name: name, score: score, date: new Date() })
  highscores.sort((e1, e2) => e1.score > e2.score)
  highscores = highscores.slice(0, 5)
  localStorage.setItem('highscores', JSON.stringify(highscores))
  updateHighScores()
  exitToMenu()
}

function gameRender() {
  drawingService.clear()
  drawingService.drawBackground(backgroundImg)
  myCharacter.render()
  characterText.render()
  drawingService.drawPath(terrain)
  drawingService.drawPathSafe(terrain)
  particleSystem.render()
  fpsCounter.render()
}

function renderCountdown() {
  drawingService.clearBlack()
  drawingService.fillText(Math.floor(countDownNumber / 1000), CANVAS_HEIGHT / 2, CANVAS_HEIGHT / 2, 20, "50px consolas", "white")
}

function gameLoop(timestamp) {
  elapsedTime = timestamp - prevTime
  processInput(elapsedTime)
  update(timestamp, elapsedTime)
  render()
  prevTime = timestamp

  if (continueGameLoop)
    requestAnimationFrame(gameLoop)
  else {
    removeListeners()
    inputEvents.unregisterAll()
    myCharacter.reset()
  }
}
