# Simple Maze Game

Play using `wasd` or `ijkl` or the arrow keys.

The goal is to get the bird to the tree.

## Credit

Tree Sprite: [William.Thompsonj](https://opengameart.org/content/lpc-tree-recolors)

Sky Tile: [Mepavi @ You're Perfect Studio](https://opengameart.org/content/sky-tiles)

Cardinal: ["[LPC] Birds" by bluecarrot16, commissioned by castelonia](https://opengameart.org/content/lpc-birds)
