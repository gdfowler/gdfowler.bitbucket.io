let inputBuffer = {}
let canvas = null
let context = null

let hint = { x: -1, y: -1 }
let showHint = false
let hintCheckbox = null

let breadcrumbs = []
let showBreadcrumbs = false
let breadcrumbsCheckbox = null

let pathToFinish = []
let showPathToFinish = false
let pathToFinishCheckbox = null

let drawtiles = false
let drawTilesCheckbox = null

const COORD_SIZE = 1024
let MAZE_SIZE = 5

let maze = null
let myCharacter = null
var solution = []



let imgFloorTile = new Image()
imgFloorTile.isReady = false
imgFloorTile.onload = function () {
    this.isReady = true
};
imgFloorTile.src = 'skyFull.png'

let imgFloorFull = new Image()
imgFloorFull.isReady = false
imgFloorFull.onload = function () {
    this.isReady = true
};
imgFloorFull.src = 'skyFull.png'

let treeImg = new Image()
treeImg.isReady = false
treeImg.onload = function () {
    this.isReady = true
};
treeImg.src = 'tree.png'


let time = function createTime() {
    let that = {}
    that.time = 0
    that.lastEnd = 0

    that.update = (time) => {
        that.time = time
    }

    that.reset = () => {
        that.lastEnd = performance.now()
    }

    that.render = () => {
        let secs = Math.round((that.time - that.lastEnd) / 1000).toString()
        document.getElementById('time').innerHTML = secs.toHHMMSS()
    }

    return that
}()

let score = function createTime() {
    let that = {}
    that.score = 0

    that.update = () => {
        that.score = (100 * MAZE_SIZE) - Math.round((time.time - time.lastEnd) / 1000)
        that.score -= pathToFinish.length * 5
        that.score -= (pathToFinish.length - breadcrumbs.length) * 3
        if (that.score < 0) {
            that.score = 0
        }
        return that.score
    }

    that.reset = () => {
        that.score = 0
    }

    that.render = () => {
        document.getElementById('score').innerHTML = Number(that.score).padZeros()
    }

    return that
}()

function drawCell(cell, drawTiles) {

    if (drawTiles) {
        if (imgFloorTile.isReady) {
            context.drawImage(imgFloorTile,
                cell.x * (COORD_SIZE / MAZE_SIZE), cell.y * (COORD_SIZE / MAZE_SIZE),
                COORD_SIZE / MAZE_SIZE + 0.5, COORD_SIZE / MAZE_SIZE + 0.5);
        }
    }

    if (cell.edges.n === null) {
        context.moveTo(cell.x * (COORD_SIZE / MAZE_SIZE), cell.y * (COORD_SIZE / MAZE_SIZE));
        context.lineTo((cell.x + 1) * (COORD_SIZE / MAZE_SIZE), cell.y * (COORD_SIZE / MAZE_SIZE));
    }

    if (cell.edges.s === null) {
        context.moveTo(cell.x * (COORD_SIZE / MAZE_SIZE), (cell.y + 1) * (COORD_SIZE / MAZE_SIZE));
        context.lineTo((cell.x + 1) * (COORD_SIZE / MAZE_SIZE), (cell.y + 1) * (COORD_SIZE / MAZE_SIZE));
    }

    if (cell.edges.e === null) {
        context.moveTo((cell.x + 1) * (COORD_SIZE / MAZE_SIZE), cell.y * (COORD_SIZE / MAZE_SIZE));
        context.lineTo((cell.x + 1) * (COORD_SIZE / MAZE_SIZE), (cell.y + 1) * (COORD_SIZE / MAZE_SIZE));
    }

    if (cell.edges.w === null) {
        context.moveTo(cell.x * (COORD_SIZE / MAZE_SIZE), cell.y * (COORD_SIZE / MAZE_SIZE));
        context.lineTo(cell.x * (COORD_SIZE / MAZE_SIZE), (cell.y + 1) * (COORD_SIZE / MAZE_SIZE));
    }

}

function renderCharacter(character) {
    if (character.image.isReady) {
        context.drawImage(
            character.image,
            character.location.x * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            character.location.y * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            (COORD_SIZE / MAZE_SIZE) / 1.5,
            (COORD_SIZE / MAZE_SIZE) / 1.5
        );
    }
}

function renderBreadcrumbs(character, breadcrumbs) {
    if (character.image.isReady) {
        context.save()
        context.globalAlpha = 0.3
        for (let i = 0; i < breadcrumbs.length; i++) {
            context.drawImage(
                character.image,
                breadcrumbs[i].x * (COORD_SIZE / MAZE_SIZE),
                breadcrumbs[i].y * (COORD_SIZE / MAZE_SIZE),
                (COORD_SIZE / MAZE_SIZE) / 1.5,
                (COORD_SIZE / MAZE_SIZE) / 1.5
            );
        }
        context.restore()
    }
}

function renderShortestPath(character, shortestPath) {
    if (character.image.isReady) {
        context.save()
        context.filter = 'hue-rotate(200deg) brightness(150%)'
        // context.globalAlpha = 0.3
        for (let i = 0; i < shortestPath.length; i++) {
            context.drawImage(
                character.image,
                shortestPath[i].x * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
                shortestPath[i].y * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
                (COORD_SIZE / MAZE_SIZE) / 1.5,
                (COORD_SIZE / MAZE_SIZE) / 1.5
            );
        }
        context.restore()
    }
}

function renderHint(character, shortestPath) {
    if (character.image.isReady && shortestPath.length > 2) {
        context.save()
        context.filter = 'hue-rotate(200deg) brightness(150%)'
        // context.globalAlpha = 0.3
        context.drawImage(
            character.image,
            shortestPath[shortestPath.length - 2].x * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            shortestPath[shortestPath.length - 2].y * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            (COORD_SIZE / MAZE_SIZE) / 1.5,
            (COORD_SIZE / MAZE_SIZE) / 1.5
        );
        context.restore()
    } else {
        context.save()
        context.filter = 'hue-rotate(200deg) brightness(150%)'
        // context.globalAlpha = 0.3
        context.drawImage(
            character.image,
            (MAZE_SIZE - 1) * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            (MAZE_SIZE - 1) * (COORD_SIZE / MAZE_SIZE) + (COORD_SIZE / MAZE_SIZE) / MAZE_SIZE,
            (COORD_SIZE / MAZE_SIZE) / 1.5,
            (COORD_SIZE / MAZE_SIZE) / 1.5
        );
        context.restore()
    }
}

function moveCharacter(key, character) {
    if (key === 'ArrowDown' || key === 's' || key === 'k') {
        if (character.location.edges.s) {
            breadcrumbs.pushUnique({ x: character.location.x, y: character.location.y })
            character.location = character.location.edges.s;
        }
    }
    if (key == 'ArrowUp' || key === 'w' || key === 'i') {
        if (character.location.edges.n) {
            breadcrumbs.pushUnique({ x: character.location.x, y: character.location.y })
            character.location = character.location.edges.n;
        }
    }
    if (key == 'ArrowRight' || key === 'd' || key === 'l') {
        if (character.location.edges.e) {
            breadcrumbs.pushUnique({ x: character.location.x, y: character.location.y })
            character.location = character.location.edges.e;
        }
    }
    if (key == 'ArrowLeft' || key === 'a' || key === 'j') {
        if (character.location.edges.w) {
            breadcrumbs.pushUnique({ x: character.location.x, y: character.location.y })
            character.location = character.location.edges.w;
        }
    }
    if (key == 'h') {
        showHint = !showHint
        hintCheckbox.checked = showHint
    }
    if (key == 'b') {
        showBreadcrumbs = !showBreadcrumbs
        breadcrumbsCheckbox.checked = showBreadcrumbs
    }
    if (key == 'p') {
        showPathToFinish = !showPathToFinish
        pathToFinishCheckbox.checked = showPathToFinish
    }
    if (key == 'z') {
        drawTiles = !drawTiles
        drawTilesCheckbox.checked = drawTiles
    }
}

function renderMaze() {
    context.strokeStyle = 'rgb(255, 255, 255)'
    context.lineWidth = 6

    if (!drawTiles) {
        if (imgFloorFull.isReady) {
            context.drawImage(imgFloorFull,
                0, 0,
                COORD_SIZE + 0.5, COORD_SIZE + 0.5)
        }
    }

    context.beginPath()
    for (let row = 0; row < MAZE_SIZE; row++) {
        for (let col = 0; col < MAZE_SIZE; col++) {
            drawCell(maze[row][col], drawTiles)
        }
    }
    context.stroke()

    context.drawImage(
        treeImg,
        (MAZE_SIZE - 1) * (COORD_SIZE / MAZE_SIZE),
        (MAZE_SIZE - 1) * (COORD_SIZE / MAZE_SIZE),
        COORD_SIZE / MAZE_SIZE,
        COORD_SIZE / MAZE_SIZE
    )

    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(COORD_SIZE - 1, 0);
    context.lineTo(COORD_SIZE - 1, COORD_SIZE - 1);
    context.lineTo(0, COORD_SIZE - 1);
    context.closePath();
    context.strokeStyle = 'rgb(0, 0, 0)';
    context.stroke();
}

function createCharacter(imageSource, location) {
    let image = new Image();
    image.isReady = false;
    image.onload = function () {
        this.isReady = true;
    };
    image.src = imageSource;
    return {
        location: location,
        image: image
    };
}

function render() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    renderMaze();
    if (showBreadcrumbs) {
        renderBreadcrumbs(myCharacter, breadcrumbs)
    }
    if (showPathToFinish) {
        renderShortestPath(myCharacter, pathToFinish)
    }
    if (showHint) {
        renderHint(myCharacter, pathToFinish)
    }
    renderCharacter(myCharacter);
    highScores.render()
    time.render()
    score.render()
}

function processInput() {
    for (input in inputBuffer) {
        moveCharacter(inputBuffer[input], myCharacter);
    }
    inputBuffer = {};
}

function update(timestamp) {
    time.update(timestamp)
    score.update()
    solveMaze(myCharacter.location)
    if (myCharacter.location === maze[MAZE_SIZE - 1][MAZE_SIZE - 1]) {
        let name = window.prompt('What your name?')
        if (!name) name = 'Anonymous'
        highScores.add(name, score.score)
        maze = mazeManager.createMaze(MAZE_SIZE)
        myCharacter.location = maze[0][0]
        // fix time reset issue
        time.reset()
        score.reset()
        breadcrumbs = []
    }
}

function gameLoop(timestamp) {
    processInput();
    update(timestamp)
    render();

    requestAnimationFrame(gameLoop);
}

function initialize() {
    canvas = document.getElementById('canvas-main');
    context = canvas.getContext('2d');

    hintCheckbox = document.getElementById('hint')
    showHint = hintCheckbox.checked
    hintCheckbox.onchange = () => showHint = !showHint

    breadcrumbsCheckbox = document.getElementById('breadcrumbs')
    showBreadcrumbs = breadcrumbsCheckbox.checked
    breadcrumbsCheckbox.onchange = () => showBreadcrumbs = !showBreadcrumbs

    pathToFinishCheckbox = document.getElementById('shortestPath')
    showPathToFinish = pathToFinishCheckbox.checked
    pathToFinishCheckbox.onchange = () => showPathToFinish = !showPathToFinish

    drawTilesCheckbox = document.getElementById('bgImg')
    drawTiles = drawTilesCheckbox.checked
    drawTilesCheckbox.onchange = () => drawTiles = !drawTiles

    highScores.init()

    window.addEventListener('keydown', function (event) {
        inputBuffer[event.key] = event.key;
    });

    mazeManager = createMaze()
    maze = mazeManager.createMaze(MAZE_SIZE)

    myCharacter = createCharacter('cardinalCharacter.png', maze[0][0])

    solveMaze(myCharacter.location)

    requestAnimationFrame(gameLoop);
}

function changeMazeSize() {
    if (document.getElementById('size5').checked) {
        MAZE_SIZE = 5
    }
    else if (document.getElementById('size10').checked) {
        MAZE_SIZE = 10
    }
    else if (document.getElementById('size15').checked) {
        MAZE_SIZE = 15
    }
    else if (document.getElementById('size20').checked) {
        MAZE_SIZE = 20
    }
    else {
        MAZE_SIZE = 5
    }
    maze = mazeManager.createMaze(MAZE_SIZE)
    breadcrumbs = []
    time.reset()
    score.reset()
    myCharacter.location = maze[0][0]
}

function solveMaze(location) {
    pathToFinish = []
    mazeManager.resetDiscovered()
    solveHelper(location)
}

function solveHelper(cell) {
    if (cell === null || cell.discovered) {
        return false
    }
    else {
        cell.discovered = true
        if (cell.x === MAZE_SIZE - 1 && cell.y === MAZE_SIZE - 1) {
            pathToFinish.push(cell)
            return true
        }
        for (direction in cell.edges) {
            if (solveHelper(cell.edges[direction])) {
                pathToFinish.push(cell)
                return true
            }
        }
    }
}
