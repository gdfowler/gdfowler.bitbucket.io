let highScores = function createHighScores() {
  let that = {}
  that.highScores = []
  that.html = null

  that.init = () => {
    that.html = document.getElementById('highScores')
    let scores = localStorage.getItem('highScores')
    that.highScores = scores ? JSON.parse(scores) : []
    that.highScores.sort((a, b) => a.score < b.score)
    that.highScores = that.highScores.slice(0, 9)
  }

  that.render = () => {
    let scoreString = ''
    for (let i = 0; i < that.highScores.length; i++) {
      scoreString += `${that.highScores[i].name}: ${that.highScores[i].score} (${that.highScores[i].size}x${that.highScores[i].size}) </br></br>`
    }
    that.html.innerHTML = scoreString
  }

  that.add = (name, score) => {
    that.highScores.push({ name: name, score: score, size: MAZE_SIZE })
    that.highScores.sort((a, b) => a.score < b.score)
    that.highScores = that.highScores.slice(0, 9)
    localStorage.setItem('highScores', JSON.stringify(that.highScores))
  }

  return that
}()