// Adapted from https://stackoverflow.com/a/6274398/10416065
Array.prototype.shuffle = function () {
  let array = this.slice()
  let counter = array.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);
    counter--;
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

Array.prototype.pushUnique = function (item) {
  let isDup = false
  for (let i = 0; i < this.length; i++) {
    if (_.isEqual(this[i], item)) {
      isDup = true
      break
    }
  }
  if (!isDup) {
    this.push(item)
    return this
  }
  else return this
}

// from https://stackoverflow.com/a/6313008/10416065
String.prototype.toHHMMSS = function () {
  var sec_num = parseInt(this, 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours < 10) { hours = "0" + hours; }
  if (minutes < 10) { minutes = "0" + minutes; }
  if (seconds < 10) { seconds = "0" + seconds; }
  return hours + ':' + minutes + ':' + seconds;
}

Number.prototype.padZeros = function () {
  let numZeros = 4
  let number = this.toString()
  return number <= 9999 ? `000${number}`.slice(-(numZeros)) : number;
}