function createMaze() {
  let that = {}
  that.maze = []
  that.mazeSize = -1
  that.frontier = [] // that.frontier.push({x: 0, y: 0})
  that.mazeCellsList = [] // that.mazeCellsList.push({x: 0, y: 0}) // takes x,y from frontier cell

  that.randomDirection = (x, y) => {
    let direction = []
    if (that.checkBounds(x - 1, y)) {
      if (that.maze[x - 1][y].inMaze) {
        direction.push({ direction: 'n', cell: that.maze[x - 1][y] })
      }
    }
    if (that.checkBounds(x, y - 1)) {
      if (that.maze[x][y - 1].inMaze) {
        direction.push({ direction: 'w', cell: that.maze[x][y - 1] })
      }
    }
    if (that.checkBounds(x + 1, y)) {
      if (that.maze[x + 1][y].inMaze) {
        direction.push({ direction: 's', cell: that.maze[x + 1][y] })
      }
    }
    if (that.checkBounds(x, y + 1)) {
      if (that.maze[x][y + 1].inMaze) {
        direction.push({ direction: 'e', cell: that.maze[x][y + 1] })
      }
    }
    direction = direction.shuffle()
    return direction[0]
  }

  that.checkBounds = (x, y) => {
    return x >= 0 && y >= 0 && x < that.mazeSize && y < that.mazeSize
  }

  that.addToFrontier = (x, y) => {
    let cell = { x: x, y: y }
    let cells = []

    if (x - 1 > 0) {
      cells.push({ x: x - 1, y: y })
    }
    if (y - 1 > 0) {
      cells.push({ x: x, y: y - 1 })
    }
    if (x + 1 < that.mazeSize) {
      cells.push({ x: x + 1, y: y })
    }
    if (y + 1 < that.mazeSize) {
      cells.push({ x: x, y: y + 1 })
    }

    let cellInFrontier = false

    for (let i = 0; i < cells.length; i++) {
      cell = cells[i]
      cellInFrontier = false

      // adds neighbors of x,y to frontier (if not in mazeCellsList or frontier already)
      for (let j = 0; j < that.frontier.length; j++) {
        if (_.isEqual(that.frontier[j], cell)) {
          cellInFrontier = true
        }
      }

      if (!that.maze[cell.x][cell.y].inMaze) {
        that.frontier.pushUnique({ x: cell.x, y: cell.y })
      }
    }

  }

  that.getNextCell = () => {
    that.frontier = that.frontier.shuffle()
    return that.frontier.pop()
  }

  that.printMaze = () => {
    console.log('------------that.maze------------')
    console.log(`Maze size: ${that.mazeSize}`)
    console.log(that.maze)
    console.log('!------------that.maze------------')
  }

  that.printFrontier = () => {
    console.log('__________that.frontier_________')
    console.log(that.frontier)
    console.log('!__________that.frontier_________')
  }

  that.createMaze = function (size) {
    that.mazeSize = size
    that.resetMaze()
    that.generateMaze()
    return that.maze
  }

  that.testFrontier = () => {
    that.addToFrontier(0, 0)
    that.addToFrontier(0, 1)
    that.addToFrontier(1, 0)
    that.addToFrontier(2, 2)
    while (that.frontier.length > 0) {
      console.log(that.getNextCell())
    }
    that.printFrontier()
  }

  that.resetMaze = function () {
    that.maze = []
    for (let row = 0; row < that.mazeSize; row++) {
      that.maze.push([])
      for (let col = 0; col < that.mazeSize; col++) {
        that.maze[row].push({
          x: col,
          y: row,
          inMaze: false,
          discovered: false,
          edges: {
            n: null,
            s: null,
            w: null,
            e: null
          }
        })
      }
    }
  }

  that.resetDiscovered = () => {
    for (let row = 0; row < that.mazeSize; row++) {
      for (let col = 0; col < that.mazeSize; col++) {
        that.maze[row][col].discovered = false
      }
    }
  }

  that.backtrace = (direction, cellA, cellB) => {
    if (direction === 'n') {
      cellB.edges['s'] = cellA
    }
    if (direction === 's') {
      cellB.edges['n'] = cellA
    }
    if (direction === 'e') {
      cellB.edges['w'] = cellA
    }
    if (direction === 'w') {
      cellB.edges['e'] = cellA
    }
  }

  that.generateMaze = () => {
    that.maze[0][0].inMaze = true
    that.addToFrontier(0, 0)
    let prevCell = that.maze[0][0]
    let nextCell = null
    let cellAndDir = null

    while (that.frontier.length > 0) {
      nextCell = that.getNextCell()
      cellAndDir = that.randomDirection(nextCell.x, nextCell.y)

      that.maze[nextCell.x][nextCell.y].edges[cellAndDir.direction] = cellAndDir.cell
      that.backtrace(cellAndDir.direction, that.maze[nextCell.x][nextCell.y], cellAndDir.cell)

      that.maze[nextCell.x][nextCell.y].inMaze = true
      that.addToFrontier(nextCell.x, nextCell.y)
    }
  }

  return that
}